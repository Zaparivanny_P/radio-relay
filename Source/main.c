#include "stm8s.h"

#define BASE_EEPROM 0x4000
#define KEY (GPIO_ReadInputData(GPIOB) & GPIO_PIN_4)
#define OUT (GPIO_ReadInputData(GPIOB) & GPIO_PIN_2)
//(GPIO_ReadInputData(GPIOB) & GPIO_PIN_2)

char out_en = 0;
int tmp, ch, cmp1, cmp2, cmp3;
char hi_byte, mode=0, tmp_del;
char buf[0x0F];
char tmp_buf, buf_cnt;
int tmp_ch;
char e_mode;


void run(void);

int main(void)
{
    run();
    return 0;
}

void delay_ms(int ms)
{
  
}

void write_out(char in)
{
    char tmp = GPIO_ReadInputData(GPIOB);
    tmp &= ~(GPIO_PIN_2);
    tmp |= GPIO_PIN_2 * in;
    GPIO_Write(GPIOB, tmp);
}

void blink(void)
{
    write_out(1);
    delay_ms(400);
    write_out(0);
    delay_ms(400);
}

void stick(void)
{             
    delay_ms(400);
    while(ch < 1700);  //���� ����� ����� 
    delay_ms(400);
    while(ch > 1700);  // ���� ����� ����
}

//add setting compare

void run()
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);

    // Input/Output Ports initialization
    // Port B initialization
    // Func5=In Func4=In Func3=In Func2=Out Func1=Out Func0=In 
    // State5=T State4=P State3=P State2=0 State1=0 State0=T 
    //PORTB=0x18;
    //DDRB=0x06;

    GPIO_Init(GPIOB, GPIO_PIN_5 | GPIO_PIN_4 | GPIO_PIN_3 | GPIO_PIN_0, GPIO_MODE_IN_PU_IT);
    GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_SLOW);

    TIM2_TimeBaseInit(TIM2_PRESCALER_16, 0xff);
    TIM2_SetCompare1(0x80);

    
    TIM2_TimeBaseInit(TIM2_IT_UPDATE | TIM2_IT_CC1, ENABLE);
    TIM2_Cmd(ENABLE);
    enableInterrupts();
    
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOB, EXTI_SENSITIVITY_RISE_FALL);
    EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_FALL_ONLY);

    out_en = 0;
    delay_ms(500);    // ���� ����������� ��������� ���� �� �����
    
    FLASH_Unlock(FLASH_MEMTYPE_DATA);
    while (KEY)
    {// ���� ��������� ��������� - �� ��� ���� � �����
         delay_ms(500);
         while (KEY)      // ������, ���� ������������� ���������
         {
            stick(); // // ����� ���� - ����    
            e_mode = 0;           // ��������� � �������
            blink();            // ���� ������� - ���-����

            stick();       
            e_mode = 1;           // ��� ������� - ������
            blink(); 
            blink();

            stick();        
            e_mode = 2;           // ��� ������� - �������
            blink(); 
            blink();
            blink();

            stick();   
            e_mode = 3;           // ������ - ��� � ������� � 2 ���
            blink();
            blink();
            blink();
            blink();
        }
    }
    FLASH_ProgramByte(BASE_EEPROM, e_mode);
    FLASH_Lock(FLASH_MEMTYPE_DATA);

    mode = e_mode; // ����� �� ������� � ���
    delay_ms(500);    // ���� ����������� ��������� ���� �� �����
    ch = 1200;
    out_en = 1;         // ��������� ������ ������
    delay_ms(100);

    while (1)
    {
        switch (mode)   // ����� ��������� �� ������ (ch - ��� ������������ ���������� �������� �� 1200 �� 2200 ��.)
        {
            case 0:
            if (ch > 1700) write_out(1); // �������� - ���������
            else write_out(0);
            break;
            
            case 1:
            if (ch > 2300) ch = 2300; 
            if (ch < 1300) ch = 1300; // ������ 
            buf[buf_cnt] = 0xFF - (char)((ch - 1299) / 4);
            buf_cnt++;
            if(buf_cnt > 0x0F) buf_cnt = 0;
            tmp_ch = 0;
            for(tmp_buf = 0; tmp_buf < 0x0F; tmp_buf++)
            {
                tmp_ch = tmp_ch + buf[tmp_buf];
            }
            tmp_buf = (char)(tmp_ch / 0x0f);
            if (tmp_buf > 254) tmp_buf = 254; if(tmp_buf < 1) tmp_buf = 1; // ������ 
            TIM2_SetCompare1(tmp_buf);
            delay_ms(15);
            break;

            case 2: 
            if(ch > 1700)
            {
                if(OUT) write_out(0);
                else write_out(1);
                delay_ms(500);
            } // �������
            else write_out(0);
            break;
                                                                        
            case 3:    // � ��������� ������������  2 �������
            if(ch > 1700) 
            {
                tmp_del++; 
                delay_ms(50);
            }
            else 
                tmp_del = 0;
            if(tmp_del > 40) 
            {
                tmp_del = 41; 
                write_out(1);
            }
            else write_out(0);
            break;
  
            default:   // ���� ������ ������� �� ������� � ���-����
                mode = 0;
                FLASH_Unlock(FLASH_MEMTYPE_DATA);
                e_mode = 0;
                FLASH_ProgramByte(BASE_EEPROM, e_mode);
                FLASH_Lock(FLASH_MEMTYPE_DATA);
            break;
            } // end switch  
      }  // end while1
}


INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
{
    if(mode == 1)
        write_out(0); //��������� ������ � ������� ����
    hi_byte++;
    TIM2_ClearITPendingBit(TIM2_IT_UPDATE);
}

INTERRUPT_HANDLER(TIM2_CAP_COM_IRQHandler, 14)
{
// �������� ������ � ������� ���� � � ����������� �������
    if((mode == 1) && (TIM2_GetCapture1() < 250) && (out_en))
        write_out(1);
    TIM2_ClearITPendingBit(TIM2_IT_CC1);
}

INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
{
    // 0.833 ������������ �� ���
    cmp1 = ((TIM2_GetCapture1()) + (((int)hi_byte) << 8));
    cmp2 = ((TIM2_GetCapture1()) + (((int)hi_byte) << 8));
    //����� ��� ���� ��������, ����������
    if (cmp1 == cmp2) cmp3 = cmp1; // ���� ������� - ��� ���������
    else cmp3 = ((TIM2_GetCapture1()) + (((int)hi_byte) << 8));  // ���� ���- ������������
    if (((GPIO_ReadInputData(GPIOB) & GPIO_PIN_2) >> 0x2) == 1)  tmp = cmp3;  // �����
    else ch = cmp3 - tmp;      // ���� 1000 - 2000 ����������� (1200-2400 �� ���)

}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif